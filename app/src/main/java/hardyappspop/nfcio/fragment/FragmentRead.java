package hardyappspop.nfcio.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import hardyappspop.nfcio.MainActivityTab;
import hardyappspop.nfcio.R;
import hardyappspop.nfcio.adapter.AdapterTag;
import hardyappspop.nfcio.dto.DtoTagItem;

/**
 * Created by luisangelgarcia on 6/3/15.
 */
public class FragmentRead extends Fragment {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private View view;
    private RecyclerView rv;
    private AdapterTag adapterTag;
    private RecyclerView.LayoutManager layoutManager;
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_read, container, false);
        init();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void init() {
        rv = (RecyclerView) view.findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(view.getContext());
        rv.setLayoutManager(layoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
    }

    /**
     * Method to read the information from the specific tag and the specific intent.
     *
     * @param intent
     * @param tagReaded
     * @param context
     */
    public void readNFCTag(Intent intent, Tag tagReaded, Context context) {
        Ndef ndef = Ndef.get(tagReaded);
        try {
            ndef.connect();
            setUpRecyclerView();



            List<String> string = new ArrayList<>();
            // Setup just information about the tag, not information that was written in the TAG.
            
            /*
            DtoTagItem dtoTagItem = new DtoTagItem(MainActivityTab.ID_INFO, R.drawable.INFO, "Tag Type",
                    "about tag", size, string, string, string, null, null, new double[2]);

            / *




            int actionType,
            int icon,
            String title,
            String message,
            int size,
            List<String> phone,
            List<String> email,
            List<String> address,
            String nickname,
            String website,
            double[] location
            */
            class MyTag {
                public String type;
                public String size;
                public boolean isWritable;

                @Override
                public String toString() {
                    return "Type: " + type + " - Size: " + size + " - Writable? " + isWritable;
                }
            }

            MyTag myTag = new MyTag();
            myTag.type = ndef.getType();
            myTag.size = String.valueOf(ndef.getMaxSize());
            myTag.isWritable = ndef.isWritable();

            Toast.makeText(context, myTag.toString(), Toast.LENGTH_SHORT).show();

            Parcelable[] myTotalMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (myTotalMessages == null) {
                Toast.makeText(context, getResources().getString(R.string.str_error_no_data), Toast.LENGTH_SHORT).show();
                return;
            }

            NdefMessage[] ndefMessage = new NdefMessage[myTotalMessages.length];
            for (int i = 0; i < myTotalMessages.length; i++) {
                ndefMessage[i] = (NdefMessage) myTotalMessages[i];
            }

            int contRecord = 1;
            for (int i = 0; i < ndefMessage.length; i++) {
                for (NdefRecord record : ndefMessage[i].getRecords()) {
                    byte[] payload = record.getPayload();
                    String textReaded = new String(payload);
                    Toast.makeText(context, "Record: " + contRecord + "\n" + textReaded, Toast.LENGTH_SHORT).show();
                }
            }

            ndef.close();
        } catch (IOException e) {
            Toast.makeText(context, getResources().getString(R.string.str_error_read_tag), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Method to instantiate our adapter for the first time, this means that it will restart the recycler view
     * every time a tag is reading.
     */
    private void setUpRecyclerView() {
        List<DtoTagItem> items = new ArrayList<>();
        adapterTag = new AdapterTag(view.getContext(), items);
        //adapterTag.setOnClickListener(this);
        rv.setAdapter(adapterTag);
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
