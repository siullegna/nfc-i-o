package hardyappspop.nfcio.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hardyappspop.nfcio.ActionMenu;
import hardyappspop.nfcio.R;
import hardyappspop.nfcio.adapter.AdapterTag;
import hardyappspop.nfcio.callback.CallBackToWrite;
import hardyappspop.nfcio.callback.CallBackWrite;
import hardyappspop.nfcio.dto.DtoTagItem;

/**
 * Created by luisangelgarcia on 6/3/15.
 */
public class FragmentWrite extends Fragment implements View.OnClickListener, CallBackToWrite {
    // ===========================================================
    // Constants
    // ===========================================================
    private final String TAG = "FragmentWrite";
    // ===========================================================
    // Fields
    // ===========================================================
    private View view;
    private ImageButton btnNewAction;
    private CallBackWrite callBackWrite;
    private RecyclerView rv;
    private AdapterTag adapterTag;
    private RecyclerView.LayoutManager layoutManager;
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            callBackWrite = (CallBackWrite) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "Error: " + e.getMessage());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_write, container, false);
        init();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(getActivity(), ActionMenu.class));
        Animation animation
                = AnimationUtils.loadAnimation(view.getContext(), R.anim.anim_test);
        (view.findViewById(R.id.btn_new_action)).startAnimation(animation);
    }

    @Override
    public void onClickListener(View v, int position) {
        Toast.makeText(getActivity(), "Position: " + position, Toast.LENGTH_SHORT).show();
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void init() {
        btnNewAction = (ImageButton) view.findViewById(R.id.btn_new_action);
        btnNewAction.setOnClickListener(this);
        rv = (RecyclerView) view.findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(view.getContext());
        rv.setLayoutManager(layoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
        List<DtoTagItem> items = new ArrayList<>();
        adapterTag = new AdapterTag(view.getContext(), items);
        adapterTag.setOnClickListener(this);
        rv.setAdapter(adapterTag);
    }

    /**
     * Method to add a new item
     *
     * @param item
     */
    public void addItem(DtoTagItem item) {
        adapterTag.addItem(item);
    }

    /**
     * Method to remove an item
     *
     * @param position
     */
    public void removeItem(int position) {
        adapterTag.removeItem(position);
    }

    /**
     * Method to update an item in a specific position
     *
     * @param position
     * @param item
     */
    public void updateItem(int position, DtoTagItem item) {
        adapterTag.updateItem(position, item);
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
