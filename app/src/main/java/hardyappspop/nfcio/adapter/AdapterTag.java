package hardyappspop.nfcio.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import hardyappspop.nfcio.MainActivityTab;
import hardyappspop.nfcio.R;
import hardyappspop.nfcio.ViewHolder.VHTagItem;
import hardyappspop.nfcio.callback.CallBackToWrite;
import hardyappspop.nfcio.dto.DtoTagItem;

/**
 * Created by luisangelgarcia on 6/5/15.
 */
public class AdapterTag extends RecyclerView.Adapter<VHTagItem> {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private Context context;
    private CallBackToWrite callBackToWrite;
    private List<DtoTagItem> items;
    // ===========================================================
    // Constructors
    // ===========================================================

    public AdapterTag(Context context, List<DtoTagItem> items) {
        this.context = context;
        this.items = items;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    /**
     * Method where all the message is created for every element depending of the specific position
     *
     * @param position
     * @return {@link String} - the final message to show
     */
    private String getMessage(int position) {
        String message = "";
        switch (items.get(position).getActionType()) {
            case MainActivityTab.ID_PLAIN_TEXT:
            case MainActivityTab.ID_URL:
            case MainActivityTab.ID_PHONE_NUMBER:
                message = items.get(position).getMessage();
                break;
            case MainActivityTab.ID_CONTACT:
                message = items.get(position).getMessage();

                // Add the phones
                for (int i = 0; i < items.get(position).getPhone().size(); i++) {
                    if (i == 0) {
                        message += "\n" + context.getResources().getString(R.string.str_phone);
                    }
                    message += "\n" + items.get(position).getPhone().get(i);
                }

                // Add the emails
                for (int i = 0; i < items.get(position).getEmail().size(); i++) {
                    if (i == 0) {
                        message += "\n" + context.getResources().getString(R.string.str_email);
                    }
                    message += "\n" + items.get(position).getEmail().get(i);
                }

                // Add the address
                for (int i = 0; i < items.get(position).getAddress().size(); i++) {
                    if (i == 0) {
                        message += "\n" + context.getResources().getString(R.string.str_address);
                    }
                    message += "\n" + items.get(position).getAddress().get(i);
                }

                // Add the nickname
                if (items.get(position).getNickname() != null) {
                    message += "\n" + items.get(position).getNickname();
                }

                // Add the website
                if (items.get(position).getWebsite() != null) {
                    message += "\n" + items.get(position).getWebsite();
                }
                break;
            case MainActivityTab.ID_LOCATION:
                message = "(" + items.get(position).getLocation()[0] + ", " + items.get(position).getLocation()[1] + ")";
                break;
        }
        return message;
    }


    public void setOnClickListener(CallBackToWrite callBackToWrite) {
        this.callBackToWrite = callBackToWrite;
    }
    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public VHTagItem onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_to_write_item, viewGroup, false);
        return new VHTagItem(view, callBackToWrite);
    }

    @Override
    public void onBindViewHolder(VHTagItem holder, int position) {
        holder.icon.setImageResource(items.get(position).getIcon());
        holder.title.setText(items.get(position).getTitle());
        holder.message.setText(getMessage(position));
    }

    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * Method to add a new item
     *
     * @param item
     */
    public void addItem(DtoTagItem item) {
        items.add(item);
        notifyDataSetChanged();
    }

    /**
     * Method to remove an item
     *
     * @param position
     */
    public void removeItem(int position) {

    }

    /**
     * Method to update an item in a specific position
     *
     * @param position
     * @param item
     */
    public void updateItem(int position, DtoTagItem item) {

    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
