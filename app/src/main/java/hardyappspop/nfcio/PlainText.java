package hardyappspop.nfcio;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hardyappspop.nfcio.dto.DtoTagItem;
import hardyappspop.nfcio.utils.TextSizeInfo;

/**
 * Created by luisangelgarcia on 6/4/15.
 */
public class PlainText extends AppCompatActivity {
    // ===========================================================
    // Constants
    // ===========================================================
    private final String ENCODING = "UTF-8";
    // ===========================================================
    // Fields
    // ===========================================================
    private EditText etText;
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================
    private DtoTagItem getItem() {
        List<String> string = new ArrayList<>();

        TextSizeInfo textSizeInfo = new TextSizeInfo();
        textSizeInfo.setTextSize(etText.getText().toString().trim());
        int size = textSizeInfo.getSize();

        return new DtoTagItem(MainActivityTab.ID_PLAIN_TEXT, R.drawable.ic_action_plain_text_light,
                getApplicationContext().getResources().getString(R.string.str_plain_text) + " : " + size + " " + getResources().getString(R.string.str_subtitle_bytes),
                etText.getText().toString().trim(), size, string, string, string, null, null, new double[2]);
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plain_text);
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void init() {
        etText = (EditText) findViewById(R.id.et_text);
    }

    public void onClickAccpet(View view) {
        if (etText.getText().toString().trim().length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.str_empty_element), Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent();
        DtoTagItem dtoTagItem = getItem();
        intent.putExtra("data", dtoTagItem);
        setResult(MainActivityTab.ID_PLAIN_TEXT, intent);
        finish();
    }

    public void onClickCancel(View view) {
        Intent intent = new Intent();
        setResult(MainActivityTab.ID_NULL, intent);
        finish();
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
