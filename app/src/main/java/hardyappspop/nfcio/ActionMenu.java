package hardyappspop.nfcio;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

import hardyappspop.nfcio.sharedprefences.MySharedPreferences;

/**
 * Created by luisangelgarcia on 6/3/15.
 */
public class ActionMenu extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private View viewContainer;
    private ImageButton btnPlainText;
    private ImageButton btnURL;
    private ImageButton btnContact;
    private ImageButton btnPhoneNumber;
    private ImageButton btnLocation;
    private ImageButton btnCancel;
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_menu);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v == viewContainer) {
            finish();
            MySharedPreferences mySharedPreferences = new MySharedPreferences(this);
            mySharedPreferences.putInteger(getResources().getString(R.string.sp_id_action), MainActivityTab.ID_NULL);
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        MySharedPreferences mySharedPreferences = new MySharedPreferences(this);
        if (v == btnPlainText) {
            mySharedPreferences.putInteger(getResources().getString(R.string.sp_id_action),
                    MainActivityTab.ID_PLAIN_TEXT);
        } else if (v == btnURL) {
            mySharedPreferences.putInteger(getResources().getString(R.string.sp_id_action),
                    MainActivityTab.ID_URL);
        } else if (v == btnContact) {
            mySharedPreferences.putInteger(getResources().getString(R.string.sp_id_action),
                    MainActivityTab.ID_CONTACT);
        } else if (v == btnPhoneNumber) {
            mySharedPreferences.putInteger(getResources().getString(R.string.sp_id_action),
                    MainActivityTab.ID_PHONE_NUMBER);
        } else if (v == btnLocation) {
            mySharedPreferences.putInteger(getResources().getString(R.string.sp_id_action),
                    MainActivityTab.ID_LOCATION);
        } else if (v == btnCancel) {
            mySharedPreferences.putInteger(getResources().getString(R.string.sp_id_action), MainActivityTab.ID_NULL);
        }
        finish();
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void init() {
        viewContainer = findViewById(R.id.view_container);
        btnPlainText = (ImageButton) findViewById(R.id.btn_plain_text);
        btnURL = (ImageButton) findViewById(R.id.btn_url);
        btnContact = (ImageButton) findViewById(R.id.btn_contact);
        btnPhoneNumber = (ImageButton) findViewById(R.id.btn_phone_number);
        btnLocation = (ImageButton) findViewById(R.id.btn_location);
        btnCancel = (ImageButton) findViewById(R.id.btn_cancel);
        setupEvents();
    }

    private void setupEvents() {
        viewContainer.setOnTouchListener(this);
        btnPlainText.setOnClickListener(this);
        btnURL.setOnClickListener(this);
        btnContact.setOnClickListener(this);
        btnPhoneNumber.setOnClickListener(this);
        btnLocation.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
