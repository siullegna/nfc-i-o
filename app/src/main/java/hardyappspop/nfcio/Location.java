package hardyappspop.nfcio;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by luisangelgarcia on 6/4/15.
 */
public class Location extends AppCompatActivity {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void init() {

    }

    public void onClickAccpet(View view) {
        Intent intent = new Intent();
        intent.putExtra("data", "Location");
        setResult(MainActivityTab.ID_PLAIN_TEXT, intent);
        finish();
    }

    public void onClickCancel(View view) {
        Intent intent = new Intent();
        setResult(MainActivityTab.ID_NULL, intent);
        finish();
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
