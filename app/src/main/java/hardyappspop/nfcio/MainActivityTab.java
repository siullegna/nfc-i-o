package hardyappspop.nfcio;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.FormatException;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.Toast;

import java.io.IOException;

import hardyappspop.nfcio.callback.CallBackRead;
import hardyappspop.nfcio.callback.CallBackWrite;
import hardyappspop.nfcio.dto.DtoTagItem;
import hardyappspop.nfcio.fragment.FragmentRead;
import hardyappspop.nfcio.fragment.FragmentWrite;
import hardyappspop.nfcio.sharedprefences.MySharedPreferences;
import hardyappspop.nfcio.utils.NfcManager;

/**
 * Created by luisangelgarcia on 6/3/15.
 */
public class MainActivityTab extends AppCompatActivity implements CallBackRead, CallBackWrite {
    // ===========================================================
    // Constants
    // ===========================================================
    private final String TAG = "TabActivity";
    private final int TAB_READ = 0;
    private final int TAB_WRITE = 1;

    public static final int ID_NULL = -1;
    public static final int ID_PLAIN_TEXT = 1;
    public static final int ID_URL = 2;
    public static final int ID_CONTACT = 3;
    public static final int ID_PHONE_NUMBER = 4;
    public static final int ID_LOCATION = 5;
    public static final int ID_INFO = 6;
    // ===========================================================
    // Fields
    // ===========================================================
    private FragmentTabHost tabHost;
    private NfcAdapter nfcAdapter;
    private Tag tag;

    private PendingIntent pendingIntent;
    private IntentFilter writeTagFilters[];

    private Menu menu;
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    private boolean isReadTab() {
        return TAB_READ == tabHost.getCurrentTab();
    }

    private boolean isWriteTab() {
        return TAB_WRITE == tabHost.getCurrentTab();
    }
    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        WriteModeOn();
        if (isReadTab()) {

        } else if (isWriteTab()) {
            MySharedPreferences mySharedPreferences = new MySharedPreferences(this);
            setupAction(mySharedPreferences.getInteger(getResources().getString(R.string.sp_id_action), ID_NULL));
            mySharedPreferences.putInteger(getResources().getString(R.string.sp_id_action), ID_NULL);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        WriteModeOff();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MySharedPreferences mySharedPreferences = new MySharedPreferences(this);
        mySharedPreferences.putInteger(getResources().getString(R.string.sp_text_size), 0);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            if (isReadTab()) {
                FragmentRead fragmentRead = (FragmentRead)
                        getSupportFragmentManager()
                                .findFragmentByTag(getResources().getString(R.string.str_read_tag));
                if (fragmentRead == null) {
                    return;
                }
                fragmentRead.readNFCTag(intent, tag, this);
            } else if (isWriteTab()) {
                Toast.makeText(this, "TAB_WRITE", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "else", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void writeText(String text) {
        if (tag == null) {
            return;
        }
        try {
            NfcManager nfcManager = new NfcManager(this);
            nfcManager.write(text, tag);
        } catch (IOException e) {
            Log.e(TAG + "IOException", "Error: " + e.getMessage());
        } catch (FormatException e) {
            Log.e(TAG + "FormatExce", "Error: " + e.getMessage());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == ID_NULL) {
            return;
        }
        if (data == null || data.getExtras() == null) {
            return;
        }
        DtoTagItem dtoTagItem = data.getParcelableExtra("data");
        FragmentWrite fragmentWrite =
                (FragmentWrite) getSupportFragmentManager().findFragmentByTag(getResources()
                        .getString(R.string.str_write_tag));
        if (fragmentWrite == null) {
            return;
        }
        fragmentWrite.addItem(dtoTagItem);
        updateSubTitleToolbar(dtoTagItem.getSize());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity_tab, menu);
        this.menu = menu;
        this.menu.getItem(0).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_send_info:
                Toast.makeText(this, "Send all info", Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void init() {
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter != null) {
            if (nfcAdapter.isEnabled()) {

                tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
                tabHost.setup(this, getSupportFragmentManager(), R.id.frame_container);


                tabHost.addTab(tabHost.newTabSpec(getResources().getString(R.string.str_read_tag))
                                .setIndicator(getResources().getString(R.string.str_read_tab)),
                        FragmentRead.class, null);
                tabHost.addTab(tabHost.newTabSpec(getResources().getString(R.string.str_write_tag))
                                .setIndicator(getResources().getString(R.string.str_write_tab)),
                        FragmentWrite.class, null);

                tabHost.setOnTabChangedListener(onTabChangeListener);

                pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                        getClass()).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
                IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
                tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
                writeTagFilters = new IntentFilter[]{tagDetected};
            } else {
                // Need to activate
            }
        } else {    // No NFC supported
            Toast.makeText(this, getResources().getString(R.string.str_error_no_nfc_supported),
                    Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void WriteModeOn() {
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, writeTagFilters, null);
    }

    private void WriteModeOff() {
        nfcAdapter.disableForegroundDispatch(this);
    }

    /**
     * This method trigger one activity depending of the id.
     * Depending of the option selected from the button, you will launch different activity to fill data.
     *
     * @param id
     */
    private void setupAction(int id) {
        Intent intent;
        switch (id) {
            case ID_PLAIN_TEXT:
                intent = new Intent(this, PlainText.class);
                break;
            case ID_URL:
                intent = new Intent(this, Url.class);
                break;
            case ID_CONTACT:
                intent = new Intent(this, Contact.class);
                break;
            case ID_PHONE_NUMBER:
                intent = new Intent(this, PhoneNumber.class);
                break;
            case ID_LOCATION:
                intent = new Intent(this, Location.class);
                break;
            case ID_NULL:
                return;
            default:
                return;
        }
        startActivityForResult(intent, id);
    }

    /**
     * This method updates the subtitle of the toolbar
     *
     * @param size
     */
    private void updateSubTitleToolbar(int size) {
        MySharedPreferences mySharedPreferences = new MySharedPreferences(this);
        int totalSize = mySharedPreferences.getInteger(getResources().getString(R.string.sp_text_size), 0);
        totalSize += size;

        mySharedPreferences.putInteger(getResources().getString(R.string.sp_text_size), totalSize);

        String subTitle = getResources().getString(R.string.str_subtitle_write) + " " + totalSize + " "
                + getResources().getString(R.string.str_subtitle_bytes);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(subTitle);
        } else if (getActionBar() != null) {
            getActionBar().setSubtitle(subTitle);
        }
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private TabHost.OnTabChangeListener onTabChangeListener = new TabHost.OnTabChangeListener() {
        @Override
        public void onTabChanged(String tabId) {
            if (tabId.equals(getResources().getString(R.string.str_read_tag))) {
                menu.getItem(0).setVisible(false);
            } else if (tabId.equals(getResources().getString(R.string.str_write_tag))) {
                menu.getItem(0).setVisible(true);
            }
        }
    };
}
