package hardyappspop.nfcio.utils;

import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

/**
 * Created by luisangelgarcia on 6/5/15.
 */
public class TextSizeInfo {
    // ===========================================================
    // Constants
    // ===========================================================
    private final String TAG = "SizeTextInfo";
    // ===========================================================
    // Fields
    // ===========================================================
    private int size;

    // ===========================================================
    // Constructors
    // ===========================================================
    public TextSizeInfo() {
        size = 0;
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================
    public void setTextSize(String text) {
        try {
            size = text.getBytes("utf8").length;
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Error: " + e.getMessage());
        }
    }

    public int getSize() {
        return size;
    }
    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
