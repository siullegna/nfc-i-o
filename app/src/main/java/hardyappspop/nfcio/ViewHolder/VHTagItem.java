package hardyappspop.nfcio.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import hardyappspop.nfcio.R;
import hardyappspop.nfcio.callback.CallBackToWrite;

/**
 * Created by luisangelgarcia on 6/5/15.
 */
public class VHTagItem extends RecyclerView.ViewHolder implements View.OnClickListener {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private CallBackToWrite callBackToWrite;
    public ImageButton icon;
    public TextView title;
    public TextView message;
    // ===========================================================
    // Constructors
    // ===========================================================
    public VHTagItem(View item, CallBackToWrite callBackToWrite) {
        super(item);
        this.callBackToWrite = callBackToWrite;
        this.icon = (ImageButton) item.findViewById(R.id.icon);
        this.title = (TextView) item.findViewById(R.id.title);
        this.message = (TextView) item.findViewById(R.id.message);
        item.setOnClickListener(this);
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void onClick(View v) {
        this.callBackToWrite.onClickListener(v, getAdapterPosition());
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
