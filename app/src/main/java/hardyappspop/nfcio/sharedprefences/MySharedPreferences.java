package hardyappspop.nfcio.sharedprefences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by luisangelgarcia on 6/4/15.
 */
public class MySharedPreferences {
    // ===========================================================
    // Constants
    // ===========================================================
    private final String SHARED_PREFERENCES = "hardyappspop.noteskeeper";
    // ===========================================================
    // Fields
    // ===========================================================
    private SharedPreferences sPreferences;
    private SharedPreferences.Editor sEditor;
    // ===========================================================
    // Constructors
    // ===========================================================
    public MySharedPreferences(Context context) {
        sPreferences = context.getSharedPreferences(this.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        sEditor = sPreferences.edit();
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    /**
     *
    */
    public void putString(String key, String defValue) {
        sEditor.putString(key, defValue);
        sEditor.commit();
    }

    /**
     *
    */
    public String getString(String key, String defValue) {
        return sPreferences.getString(key, defValue);
    }

    /**
     *
     */
    public void putInteger(String key, Integer defValue) {
        sEditor.putInt(key, defValue);
        sEditor.commit();
    }

    /**
     *
     */
    public Integer getInteger(String key, Integer defValue) {
        return sPreferences.getInt(key, defValue);
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
