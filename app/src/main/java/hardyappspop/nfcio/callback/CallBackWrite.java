package hardyappspop.nfcio.callback;

/**
 * Created by luisangelgarcia on 6/3/15.
 */
public interface CallBackWrite {
    void writeText(String text);
}
