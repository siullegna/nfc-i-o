package hardyappspop.nfcio.callback;

import android.view.View;

/**
 * Created by luisangelgarcia on 6/5/15.
 */
public interface CallBackToWrite {
    void onClickListener(View v, int position);
}
