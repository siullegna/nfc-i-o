package hardyappspop.nfcio.dto;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luisangelgarcia on 6/5/15.
 */
public class DtoTagItem implements Parcelable {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    /**
     * Type of the action
     */
    private int actionType;
    /**
     * The icon to show.
     */
    private int icon;
    /**
     * The title or header of the row
     */
    private String title;
    /**
     * All the type of actions share this field:
     * ACTION_PLAIN_TEXT
     * ACTION_URL
     * ACTION_CONTACT
     * ACTION_PHONE
     * Except:
     * ACTION_LOCATION
     */
    private String message;
    /**
     * Size in bytes of the information
     * All the types of actions share this field
     * ACTION_PLAIN_TEXT
     * ACTION_URL
     * ACTION_CONTACT
     * ACTION_PHONE
     * ACTION_LOCATION
     */
    private int size;
    /**
     * this field saves the Phone/Phones that will appear in the information for ACTION_CONTACT:
     * in this action, the phones could be one to n
     */
    private List<String> phone;
    /**
     * this field saves the email/emails for the ACTION_CONTACT
     */
    private List<String> email;
    /**
     * this field saves the address/addresses for the ACTION_CONTACT
     */
    private List<String> address;
    /**
     * this field saves the nickname for the ACTION_CONTACT
     */
    private String nickname;
    /**
     * this field saves the web site for the ACTION_CONTACT
     */
    private String website;
    /**
     * this field saves a location for the ACTION_LOCATION,
     * the for should be [lat, lon]
     */
    private double[] location;

    // ===========================================================
    // Constructors
    // ===========================================================
    private DtoTagItem() {
        phone = new ArrayList<>();
        email = new ArrayList<>();
        address = new ArrayList<>();

        location = new double[2];
    }

    private DtoTagItem(Parcel in) {
        this();
        actionType = in.readInt();
        icon = in.readInt();
        title = in.readString();
        message = in.readString();
        size = in.readInt();
        in.readStringList(phone);
        in.readStringList(email);
        in.readStringList(address);
        nickname = in.readString();
        website = in.readString();
        in.readDoubleArray(location);
    }

    public DtoTagItem(int actionType, int icon, String title, String message, int size, List<String> phone,
                      List<String> email, List<String> address, String nickname, String website,
                      double[] location) {
        this.actionType = actionType;
        this.icon = icon;
        this.title = title;
        this.message = message;
        this.size = size;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.nickname = nickname;
        this.website = website;
        this.location = location;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public int getActionType() {
        return actionType;
    }

    public void setActionType(int actionType) {
        this.actionType = actionType;
    }

    public List<String> getAddress() {
        return address;
    }

    public void setAddress(List<String> address) {
        this.address = address;
    }

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public double[] getLocation() {
        return location;
    }

    public void setLocation(double[] location) {
        this.location = location;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public List<String> getPhone() {
        return phone;
    }

    public void setPhone(List<String> phone) {
        this.phone = phone;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(actionType);
        dest.writeInt(icon);
        dest.writeString(title);
        dest.writeString(message);
        dest.writeInt(size);
        dest.writeStringList(phone);
        dest.writeStringList(email);
        dest.writeStringList(address);
        dest.writeString(nickname);
        dest.writeString(website);
        dest.writeDoubleArray(location);
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    public static final Creator<DtoTagItem> CREATOR = new Creator<DtoTagItem>() {
        @Override
        public DtoTagItem createFromParcel(Parcel source) {
            return new DtoTagItem(source);
        }

        @Override
        public DtoTagItem[] newArray(int size) {
            return new DtoTagItem[size];
        }
    };
}
